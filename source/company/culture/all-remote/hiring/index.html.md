---
layout: markdown_page
title: "All-Remote Hiring"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

On this page, we're detailing how to properly, efficiently, and effectively hire in an all-remote environment.

## How do you hire in an all-remote organization?

"How do you handle hiring in an all-remote company?" is a question we at GitLab hear often. Many companies hire only within one country or region, and even multinational corporations typically hire into specific offices. 

GitLab envisions a world where talented, driven individuals can find roles and seek employment based on business needs, rather than an oftentimes arbitrary location. 

For organizations struggling to find, recruit, retain, and compensate employees in locales such as San Francisco, New York City, London, Singapore, and Sydney, imagine the influx of highly qualfied applicants if the location requirement were removed from all job descriptions. For a glimpse at what this looks like, please browse [GitLab's vacancies](/jobs/). 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/V2Z1h_2gLNU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

You can hear from our [geographically diverse](/company/culture/inclusion/#fully-distributed-and-completely-connected) team in the video above, filmed at our all-employee gathering — [Contribute](/events/gitlab-contribute/).

We want to state up front that one of the advantages of being an all-remote company is that we can [hire talent from a global pool](/handbook/hiring/). We are not restricted to the usual job centers, which gives us access to a tremendous amount of talent that many other companies will not consider for employment. It may take more effort to find talent in more diverse places, but that is an effort we are willing to make. 

You can learn more in the [Principles of Hiring section of our Handbook](/handbook/hiring/principles/). 

### Why do you hire from anywhere?

GitLab's six [values](/handbook/values/) are Collaboration, Results, Efficiency, Diversity & Inclusion, Iteration, and Transparency, and together they spell **CREDIT**. 

True to those values, GitLab strives to hire team members who are passionate, empathetic, kind, tenacious, and ambitious, regardless of their location. By opening the recruiting funnel to as broad a swath of the world as we can, we create a more inclusive hiring environment, lean on tight collaboration to drive progress [across time zones](/company/culture/all-remote/management/#asynchronous), and focus our hiring decisions on results rather than location. 

Hiring an all-remote team from across the globe allows GitLab to pay local rates, which you can learn more about [on the company blog](/blog/2019/02/28/why-we-pay-local-rates/). By hiring brilliant minds in locations with lower costs of living, GitLab is able to save money to hire even more people as we scale our business. 

For now, this gives GitLab a tremendous competitive advantage. We are sourcing talent from places that most companies overlook, and we're creating [a more diverse team](/company/culture/inclusion/#fully-distributed-and-completely-connected) all the while. We hope that this advantage fades, as more companies embrace all-remote and widen their recruiting funnel beyond the usual talent centers.

We've published our [hiring process](/handbook/hiring/interviewing/), including example screening questions, in our handbook. 
While this may be unique, we see it as simply staying true to our [transparency value](/handbook/values/#transparency). The process shouldn't be a mystery. 

Letting candidates know what to expect allows them to focus on whether the role and the company are right for them, while we evaluate that too. 

### Where do you *not* hire?

Each country has unique and complex rules, laws and regulations, which can affect us to conduct business, as well as the employability of the citizens of those countries.

We are growing rapidly and continuously expanding our hiring capabilities in other geographies. In countries listed in our [contract_factors.yml file](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/contract_factors.yml), we have a payroll and can employ you as an employee. In all other countries, we can hire you as a contractor.

Learn more at our [Country Hiring Guidelines](/jobs/faq/#country-hiring-guidelines) page.

## Using video calls to interview and engage

At GitLab, we do not rely on in-person interviews. Instead, we utilize [Zoom](/handbook/tools-and-tips/#zoom) to connect with candidates via [video calls](/handbook/communication/#video-calls). 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/tHThOsneFOY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

In the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) video above, Nicole Schwartz and Jeremy Watson, product managers at GitLab, walk through the Deep Dive portion of the product management interview process. For more, watch Shane Bouchard, UX Manager, and Jeremy Watson, Senior Product Manager, [discuss interviewing and finding great designers](https://www.youtube.com/watch?v=d15_q_K5Yq8).

This approach reinforces several of GitLab's [values](/handbook/values/). 
* [Efficiency](/handbook/values/#efficiency): It's highly efficient, given that neither the candidate nor the interviewer must devote time to relocating in order to engage with one another. This allows us to hire faster and provide a better candidate experience.
* [Diversity & Inclusion](/handbook/values/#diversity--inclusion): By allowing candidates to interview from a space where they are comfortable, we create a level playing field for those with mobility concerns, caregivers, etc. 

Learn more in the [Interviewing section of GitLab's Handbook](/handbook/hiring/interviewing/).

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/6mZqzK_40FE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

Above, GitLab co-founder and CEO Sid Sijbrandi sits down for an interview with Sunil Kowlgi, founder of [Outklip](https://outklip.com/), on the topic of using video for effective collaboration. You can read more on their discussion — which covers remote hiring, management, customer support, and more — [on GitLab's blog](/blog/2019/04/18/lessons-on-building-a-distributed-company/).

It's important for candidates to become comfortable with video calls, as they are a primary medium for communication within GitLab. Because we are an all-remote organization with no offices, meetings and informal coffee chats occur using video calls. 

Learn more in the [Meetings](/company/culture/all-remote/meetings/) section of our All-Remote section on company culture.

## Handling local regulations, risks, entities, etc.

Hiring across the globe isn't without its challenges. There are local regulations and risks unique to countries and regions around the globe. 

Rather than attempting to incorporate in every country where an all-remote company has even a single employee, organizations must weigh the benefits of creating a legal entity with other options. There are four means of engagement with GitLab. These are commonly used arranagements that can be considered by other all-remote companies.

* **GitLab Entity**: Individuals can be employed directly with GitLab Inc, BV, LTD, GmbH, PTY, Federal.
* **Professional Employer Organization (PEO)**: In select countries where GitLab does not have an entity, we hire professional employment organizations to serve as the [employer of record (EOR)](/handbook/contracts/#employer-of-record-providers) in order to facilitate payments. 
* **C2C (Contractor)**: A corp to corp arrangement, whereby a corporation or LLC invoices GitLab BV for GitLab related work.
* **IND (Contractor)**: By far the most widely applicable. The individual contractor arrangement can be used in countries where GitLab is hiring, yet does not have an entity or PEO agreement in place. This allows an individual to invoice GitLab BV as an individual or via their own company with no partners.

Learn more about [employee types and hiring partners](/handbook/contracts/#employee-types-at-gitlab) across countries in the [Contracts](/handbook/contracts/) section of GitLab's Handbook.

It is not always practical to understand the nuances of local regulations in-house. Wherever GitLab has an entity or contractor, we have external counsel to advise and ensure that we are compliant. 

In countries where GitLab utilizes a professional employer organization (PEO), counsel within the PEO advises us to ensure hiring compliance. 

## Local sourcers

Even when the whole world is your talent pool, there will be roles that require deliberate involvement and proactive search. 

For these purposes, GitLab built a Sourcing team focused on finding best passive talent and nurturing the candidate pipeline — a tactic that can be implemented by other all-remote companies. 

While GitLab is location-agnostic, we work to ensure that we have local sourcers based in every macro-region (e.g. Americas, APAC and EMEA) so they can bring their market expertise and leverage our presence in these territories. 

We also consider our sourcing effort extremely important for bringing [diverse](/company/culture/inclusion/) talent onboard. Our sourcers are aligned with recruiters and business verticals to ensure that they can apply their unique knowledge of specific areas.

Driven by our [Collaboration value](/handbook/values/#collaboration), we organize [source-a-thons](/handbook/hiring/sourcing/#source-a-thons), which become a place where everyone can share their market insights and contribute to hiring. We find these sessions extremely productive as they help sourcers partner with their hiring managers and calibrate their expectations.

Learn more on how GitLab's Sourcing team operates [here](https://about.gitlab.com/handbook/hiring/sourcing/). 

----

Return to the main [all-remote page](/company/culture/all-remote/).
