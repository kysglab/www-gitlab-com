---
layout: markdown_page
title: "Category Vision - Secrets Management"
---

- TOC
{:toc}

## Secrets Management

[Hashicorp Vault](https://www.vaultproject.io/) is a powerful secrets management tool
that we are seeing more and more of our customers using, and that we've become big fans
of over time. Vault lets you easily rotate secrets and can manage intermediate, temporary
tokens used between different services, ensuring that there are no long-term tokens lying
around or commonly used that would be valuable to an attacker. This will add protection
against any unknown zero-day vulnerabilities in our Rails app today, as well as any
zero-day issues that would allow a bad actor to access the Gitlab server. 

Since Vault is an open source tool, our vision is to embed Vault within GitLab and migrate
to using it for our own secrets management, within the GitLab Core as well as [for your
CI Runners](https://gitlab.com/groups/gitlab-org/-/epics/816). Furthermore, this Vault
instance would be exposed for your own internal use for your own secrets. In this way we
can provide a comprehensive secrets management solution, for all of your secrets, built
right into GitLab.

See also an [introduction](https://www.youtube.com/watch?v=pURiZLl6o3w) to this feature
from our CEO @sytses.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ASecrets%20Management)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1300) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## Target audience and experience

Operations teams tasked with managing secure credentials will be the primary user of this
feature. Compliance is quite complicated, and Vault can make it much easier. Additionally,
governance teams (such as compliance, security, and audit teams) that are responsible
for managing GitLab as a corporate solution will find value here, knowing that there
is an additional layer of security built in to GitLab helping to ensure that secure
tokens and other confidential data in GitLab is even more protected than it is today.

## What's next & why

The Verify team is enabling pipeline secrets to be stored in Vault via [gitlab#28321](https://gitlab.com/gitlab-org/gitlab/issues/28321),
so we want to follow that issue quickly by allowing for Vault to be installed in our
customers' Kubernetes clusters via [gitlab#9982](https://gitlab.com/gitlab-org/gitlab/issues/9982).

Beyond this we are looking at including the Vault installation as part of the omnibus
([omnibus-gitlab#4317](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/4317))
package, which comes for free bundled with GitLab. Having an installation guaranteed
available serves as a foundation upon which we can build a deeper integration for
[moving GitLab's own secrets into Vault](https://gitlab.com/groups/gitlab-org/-/epics/1319)
as well. Adding the same capability to gitlab.com is possible, but quite complex so is
being researched in its own issue [gitlab-org#28584](https://gitlab.com/gitlab-org/gitlab/issues/28584).

## Maturity Plan

This category is currently at the "Planned" maturity level, and
our next maturity target is Minimal (see our [definitions of maturity levels](/direction/maturity/)).
Key deliverables to achieve this are:

- [Allow user to install Vault into Kubernetes cluster](https://gitlab.com/gitlab-org/gitlab/issues/9982)
- [Contribute GitLab auth method to Vault](https://gitlab.com/gitlab-org/gitlab/issues/9983)

## Competitive landscape

There are other secrets management stores in the market. There is a nice overview
of [Vault vs. KMS](https://www.vaultproject.io/docs/vs/kms.html) which contains a
lot of information about why we believe Vault is a better solution for secrets
management. We could consider in the future also supporting different solutions
such as KMS.

Additionally, [Vault Enterprise](https://www.vaultproject.io/docs/enterprise/) offers
additional sets of capabilities that will _not_ be part of the open source version
of Vault bundled with GitLab. This includes 
[replication across datacenters](https://www.vaultproject.io/docs/enterprise/replication/index.html),
[hardware security modules (HSMs)](https://www.vaultproject.io/docs/enterprise/hsm/index.html),
[seals](https://www.vaultproject.io/docs/enterprise/sealwrap/index.html),
[namespaces](https://www.vaultproject.io/docs/enterprise/namespaces/index.html),
[servicing read-only requests on HA nodes](https://www.vaultproject.io/docs/enterprise/performance-standby/index.html)
(though, the open source version does support [high-availability](https://www.vaultproject.io/docs/concepts/ha.html)),
[enterprise control groups](https://www.vaultproject.io/docs/enterprise/control-groups/index.html),
[multi-factor auth](https://www.vaultproject.io/docs/enterprise/mfa/index.html),
and [sentinel](https://www.vaultproject.io/docs/enterprise/sentinel/index.html).

For customers who want to use GitLab with the enterprise version of Vault, we need
to ensure that this is easy to switch to/use as well.

One issue that will help establish us in the competitive landscape is to offer
authentication in Vault using GitLab, via [gitlab#9983](https://gitlab.com/gitlab-org/gitlab/issues/9983).
This will help users of Vault ensure that interacting with GitLab and Vault
is easy and natural.

## Top Customer Success/Sales issue(s)

Adding a Vault instance to omnibus installations that can be used for customer
secrets is the right first place to start. Additional features will be added on,
but this will meet the goal of providing a secrets management solution with GitLab
([omnibus-gitlab#4317](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/4317)).

## Top user issue(s)

Adding a Vault instance to omnibus installations that can be used for customer
secrets is the right first place to start. Additional features will be added on,
but this will meet the goal of providing a secrets management solution with GitLab
([omnibus-gitlab#4317](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/4317)).

## Top internal customer issue(s)

Internally, once the Vault integration is available we can begin moving some of
the secrets tracked internally in GitLab to the included Vault.

- [Examine which secrets we can move to Vault from gitlab.rb](https://gitlab.com/gitlab-org/omnibus-gitlab/issues/4297)
- [Discontinue using attr_encrypted](https://gitlab.com/gitlab-org/gitlab/issues/26243)
- [Allow the `db_key_base` secret to be rotated](https://gitlab.com/gitlab-org/gitlab/issues/25332)

The MVC for migrating our internal secrets is being tracked in the epic to [move GitLab's own secrets into Vault](https://gitlab.com/groups/gitlab-org/-/epics/1319).

This does require the Vault integration being mandatory as part of the install
first.

## Top Vision Item(s)

Having a Vault instance guaranteed available is table stakes, but beyond this we
can add a proper interface to Vault [gitlab#20306](https://gitlab.com/gitlab-org/gitlab/issues/20306)
embedded in GitLab, making it easier to interact with the Vault instance. This can
be leveraged for CI/CD, but also for any secrets that a customer might have in their
platform. This will be expanded up on via [gitlab#7569](https://gitlab.com/gitlab-org/gitlab/issues/7569)
which introduces automatic rotation / dynamic secrets.
