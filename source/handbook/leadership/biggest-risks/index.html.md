---
layout: handbook-page-toc
title: "Biggest risks"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

We frequently get asked questions like:

- What are the biggest risks to the company?
- What are your biggest fears?
- What keeps you up at night?

On this page, we document the biggest risks and how we intend to mitigate them. We also have a [biggest tailwinds](/handbook/leadership/biggest-tailwinds) page to see what waves we are riding in our current market.

Biggest risks have numbers attached to them for [ease of reference, not for ranking](/handbook/communication/#numbering-is-for-reference-not-as-a-signal).

## 1. Lowering the hiring bar

We are growing rapidly (more than doubling headcount in CY2019 again), and there is pressure on departments to meet their [hiring targets](/handbook/hiring/charts/).
It is better for us to miss our targets than to hire people who won't be able to perform to our standards since that takes much longer to resolve.
To ensure the people we hire make the company better, we:

1. Have a standard interview structure
1. Review the interview scores of new hires to look for trends
1. [Identify and take action on underperformance](/handbook/underperformance)
1. (make this unneeded) Have the CPO and CEO sample new hires and review [manager](/handbook/leadership/#management-group), [staff engineer](/job-families/engineering/backend-engineer/), [principal product manager](/job-families/product/product-manager/) and up hires
1. Compare the external title with the new title being given
1. Conduct bar raiser interviews
1. Review cohort performance in the company (completion of onboarding tasks, bonuses, performance review, 360 feedback, performance indicators)

## 2. Underperformance

In a similar vein, it is important that we do not slow down, which means being very proactive in addressing [underperformance](/handbook/underperformance/). 
We should [identify and take action as early as possible](/handbook/underperformance/#identify-and-take-action-as-early-as-possible).

## 3. Ineffective onboarding

We are onboarding many people quickly, making it easy for things to fall behind.
Therefore we:

1. Measure the onboarding time
1. Measure the time to productivity in sales (ramp time) and engineering (time to first MR, MRs per engineer per month)
1. Make sure we work [handbook-first](/handbook/handbook-usage/#why-handbook-first), so the handbook is up to date.

## 4. Confusion about the expected output

As we add more layers of management to accommodate the new people, it's easy to become confused about what is expected of you.

To make sure this is clear we:

1. Document who is the [DRI](/handbook/people-group/directly-responsible-individuals/) on a decision.
1. Have clear [KPIs](/handbook/business-ops/data-team/metrics/#gitlab-kpis) across the company
1. Have [Key monthly reviews](/handbook/finance/key-monthly-metrics/)
1. Have a job family that includes performance indicators
1. Have a [clear org-chart](/company/team/org-chart/) where [each individual reports to exactly one person](/handbook/leadership/#no-matrix-organization)
1. Celebrate our growth in revenue when we hit $100M in ARR instead of 1000 people. We put our attention in celebrating our output instead of input, per our [results value](/handbook/values/#results).

## 5. Loss of the values that bind us

It's easy for a culture to get diluted if a company is growing fast.
To make our [values](/handbook/values/) stronger, we:

1. Regularly add to them and update them
1. Find new ways to [reinforce our values](/handbook/values/#how-do-we-reinforce-our-values)

## 6. Loss of the open source community

1. Keep our [promises](/company/stewardship/#promises)
1. Keep listening
1. Assign [Merge Request Coaches](/job-families/expert/merge-request-coach/)
1. Have [contributing organizations](/community/contributing-orgs/)

## 7. Loss of velocity

Most companies start shipping more slowly as they grow.
To keep our pace, we need to:

1. Ensure we get [10 Merge Requests (MRs) per engineer per month](/handbook/engineering/development/performance-indicators/#average-mrs-dev-month)
1. Have [acquired organizations](/handbook/acquisitions/) remake their functionality inside our [single application](/handbook/product/single-application/)
1. Have a [quality group](/handbook/engineering/quality/) that keeps our developer tooling efficient
1. Achieve our [category maturity targets](/handbook/product/metrics/#category-maturity-achievement)
1. Ensure each [group has non-overlapping scope](/handbook/team/structure/#product-groups)

We were voted [The World's Most Productive Remote Team](https://noonies.hackernoon.com/award/cjxvsz6576k8u0b40czyb7xhj) by HackerNoon.

## 8. Fork and commoditize

Since we are based on an open source product, there is the risk of fork and commoditize like what [AWS experienced with ElasticSearch](https://www.youtube.com/watch?v=G6ZupYzr_Zg).

This risk is reduced, because we're application software instead of infrastructure software.
Application software is less likely to be forked and commoditized for the following reasons:

| Type of software | Application software | Infrastructure software | |
| Interface | Graphical User Interface (GUI) | Application Programming Interface (API) | A GUI is harder to commoditize than an API |
| Compute usage | Drives little compute | Drives lots of compute | Hyperclouds want to drive compute |
| Deployment | Multi-tenant (GitLab.com) | Single tenant managed service (MongoDB Atlas) | Hyperclouds offer mostly managed services |
| Feature richness | Lots of features  | Few features | More features leads to more hard to commoditize propietary features |
| Ecosystem activity | Lots of contributions | Few contributions | Infrastructure is more complex to contribute to |

What we need to do is:

1. [Keep up velocity](#7-loss-of-velocity)
1. [Keep the open source community contributing](#6-loss-of-the-open-source-community)
1. [Follow our buyer-based-open-core pricing model](/handbook/ceo/pricing/#the-likely-type-of-buyer-determines-what-features-go-in-what-tier)

## 9. Security breach

Our customers entrust their application code and data to GitLab.
A security breach that erodes that trust is a significant risk.
To ensure we safeguard our customers data, we:

1. Maintain a strong [Security Operations](/handbook/engineering/security/#security-operations) team
1. Hit our Application Security [remediation SLAs](/handbook/engineering/security/#severity-and-priority-labels-on-security-issues)
1. Ensure our developers complete [secure code training](/handbook/engineering/security/#secure-coding-training)
1. Regularly perform [internal application security reviews](/handbook/engineering/security/#internal-application-security-reviews)
1. [Utilizing bug bounty programs](/handbook/engineering/security/#vulnerability-reports-and-hackerone) like HackerOne
1. Conducting [threat intelligence](/handbook/engineering/security/#threat-intelligence)
1. Have an [internal Red Team](/handbook/engineering/security/#red-team)
1. Enable our customers to secure their applications via our [Defend Stage](/handbook/product/categories/#defend-stage) categories and features

## 10. Economic Downturn

An economic downturn will likely prolong our sales cycle. Our opportunity should still be there since GitLab saves companies money on licenses and integration effort.

## 11. Competition

There will always be competitive products. 
We tend to be much more cost effective because we build on open source, iterate quickly, get open source contributions, and only have to integrate new features with GitLab instead of a large number of tool combinations.

### GitHub

After GitLab core and home-grown DIY devops platforms, GitHub is GitLab's biggest competitor. After the Microsoft acquisition they have started to follow the single application strategy pioneered by GitLab.

In order to counter this risk, GitLab will:
1. Why both are a [single application](/handbook/product/single-application/) GitLab has a much broader scope.
1. GitLab is focused on the enterprise use cases, GitHub on open source projects.
1. GitLab is independent of the hyper cloud providers and the best way to be multi-cloud.
1. Leverage our [community](/community/) to deliver new stages, categories and features faster
1. Continue to focus on [operational excellence](#operational-excellence) to out ship even substantially better financially resourced competition

### Operational excellence

We will always have competition. To deal with competition, operational excellence can be a [surprisingly durable competitive advantage](https://twitter.com/patrickc/status/1090387536520728576).

We encourage operational excellence in the following ways:

1. [Efficiency value](/handbook/values/#efficiency)
1. [Long Term Profitability Targets](/handbook/finance/financial-planning-and-analysis/#long-term-profitability-targets)
1. [KPIs](/handbook/business-ops/data-team/metrics/#gitlab-kpis)
1. Open source with a lot of wider community contributors who make it easier to uncover customer demand for features and allow our organization to stay leaner.
1. A [single application](/handbook/product/single-application/) makes the user experience better, allows us to introduce new functionality to users, and it makes it easier for us to keep our velocity.
1. Run the same code for GitLab.com and self-hosted applications and [merged the CE and EE codebases](/blog/2019/02/21/merging-ce-and-ee-codebases/)
1. How we [make decisions](/handbook/leadership/#making-decisions)

### High Ambition
Our [focus on improvement](/handbook/values/#focus-on-improvement) and commitment to [iteration](/handbook/values/#iteration) keep us rooted in what's next. This could result in us lowering our [ambition](/handbook/product/#be-ambitious). While we focus on what's next, we must also maintain a level of ambition to compete in the future in places where others might not think it is possible today.

### Serve smaller users

We have large competitors and smaller ones.
The larger competitors naturally get attention because we compete with them for large customers.
According to the [innovators dilemma](https://en.wikipedia.org/wiki/The_Innovator%27s_Dilemma): "the next generation product is not being built for the incumbent's customer set and this large customer set is not interested in the new innovation and keeps demanding more innovation with the incumbent product".
So it is really important that we also focus on the needs of smaller users since the next generation product will first be used by them.
If we don't do this we risk smaller competitors gaining marketshare there and then having the community and revenue to go up-market.

We serve smaller users by having:

1. A great free open source product that [gets the majority of new features](/company/stewardship/#promises).
1. A focus on [memory consumption reduction](/handbook/engineering/development/enablement/memory/) to ensure it is affordable to run our open source version.
1. A [tiered pricing model](/handbook/ceo/pricing/#four-tiers) with a very low pricepoint for our lowest tier.

### Infrastructure Bundling

The largest cost in application delivery is typically infrastructure. Large hyper-scale infrastructure providers could bundle their own native DevOps platform usage with their infrastructure. There are a couple of industry trends which limit this risk:
1. Businesses are increasingly avoiding infrastructure vendor lockin and pursuing multi-cloud strategies.
1. Infrastructure players have been slow to develop user friendly, complete DevOps platforms.

Also, see the [fork and commoditize](#fork-and-commoditize) move that is available to hyper-scale infrastructure providers.

## 12. Founder Departure

Often startups struggle through the transition when founders leave the company, especially when those founders also serve as the CEO.

To ensure we avoid this risk we:
1. Built a highly capable [E-Group](/handbook/leadership/#e-group) and [Board of Directors](/handbook/board-meetings/)
1. Actively discourage a cult of personality around our CEO
1. Have the CEO take normal vacations

## 13. Handbook Second

We work [Handbook First](/handbook/handbook-usage/#why-handbook-first). 
As we say, 
> Having a "handbook first" mentality ensures there is no duplication; the handbook is always up to date, and others are better able to contribute.

If we work handbook second, we risk losing these benefits. 

To ensure we avoid this risk, we:
1. Ensure all new hires read and understand the [communication guidelines](/handbook/communication/), as part of their [onboarding](https://gitlab.com/gitlab-com/people-group/employment/blob/master/.gitlab/issue_templates/onboarding.md)
1. Make the ability to coach team members to work handbook first a requirement for promotion to [Director or above](/handbook/leadership/#director-group)
1. Explicitly ask CEO Shadows to [promote working handbook first](/handbook/ceo/shadow/#promote-communication-best-practices)
1. Empower all team members to help promote our [communication guidelines](/handbook/communication/)

## 14. Key people leave 

Key people may leave as they vest and achieve their economic goals. 

As reflected in our compensation principles, we don't want people to stay because they feel like they have [golden handcuffs](/handbook/people-group/global-compensation/#why-we-pay-local-rates), but we do recognize the alignment that comes with options vesting. For team members who have been at GitLab for 3.5 years, we offer an [option refresh program](/handbook/stock-options/#option-refresh-program) through which team members are granted a new option grant. 

Alternatively, early team members may leave because working at a company the size of GitLab today or the size of GitLab in a year is different than working at an early-stage startup. 
Big companies are organizationally different than small startups, but there are many things about the spirit of a startup that we can maintain, notably:
* our [high ambition](#high-ambition)
* our [iteration value](/handbook/values/#iteration)
* [keeping up velocity](#7-loss-of-velocity)
* our [direct communication practices](/handbook/leadership/#communication-should-be-direct-not-hierarchical)

Keeping the _feel_ of a small startup, despite a growing headcount, may help retain employees who would otherwise leave. 

## 15. Innovation and creativity are stifled

As the number of layers increase and middle management layers increase, innovation and creativity are stifled. 
While this could be reflected in [loss of velocity](#7-loss-of-velocity), innovation is also about the ideas that are being brought to the table. 

In addition to [keeping our hiring bar high](#1-lowering-the-hiring-bar), we have the benefit of our community to help bring new insights and use cases creating issues. 
We can keep this momentum by continuing to value and engage with our community.
We have [Merge Request Coaches](/job-families/expert/merge-request-coach/) who help contributors to get their merge requests to meet the [contribution acceptance criteria](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#contribution-acceptance-criteria), 
and [wider community contributions per release](/handbook/marketing/community-relations/code-contributor-program/#wider-community-contributions-per-milestone) is [a GitLab KPI](/handbook/business-ops/data-team/metrics/#gitlab-kpis).

## 16. Meritocracy snuffed out by ineffective middle management

Ineffective middle management would be a result of [lowering the hiring bar](#1-lowering-the-hiring-bar). 
In addition to addressing lowering the hiring bar, we address this specific risk by:
* prioritize [our results value in our value hierarchy](/handbook/values/#hierarchy)
* encourage [discretionary bonuses for team members who perform outstandingly](/handbook/incentives/#discretionary-bonuses)
* maintain the minimum number of [layers](/company/team/structure/#organizational-chart) to be effective

## 17. Technical debt ineffectively managed

This is especially a problem if there are acquisitions of new technologies. 
We address this for acquired technology by having [acquired organizations](/handbook/acquisitions/) remake their functionality inside our [single application](/handbook/product/single-application/). 

Otherwise, we have a [clear and consistent prioritization framework across engineering](/handbook/engineering/#prioritizing-technical-decisions) and [product](/handbook/product/#prioritization) that helps ensure we are continuously making progress on the most important issues. 

## 18. Loss of customer centricity

As more folks work away from customers, it is easy to lose sight of whom we are serving. 
We can address this by:
* [living out our customer results value](/handbook/values/#customer-results)
* product managers continuing to do [customer validation cycles](/handbook/product-development-flow/#validation-track)
* [dogfooding](/handbook/values/#dogfooding) so we experience the same pain points our customers do
