---
layout: handbook-page-toc
title: Package Team
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Package Team

The Package team works on the part of GitLab concerning the [Package
stage], which integrates with [GitLab's CI/CD product](/direction/cicd/).
Our mission is to create a secure environment where both source code and dependencies can live by
allowing you to publish, consume, and discover packages of a large variety of languages and platforms
all in one place.

For more details about the vision for this area of the product, see the [product vision] page.

[product vision]: /direction/package/
[Package stage]: /direction/package/

## Team Members

The following people are permanent members of the Package Team:

<%= direct_team(manager_role: 'Engineering Manager, Package') %>

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[,&] Package/, direct_manager_role: 'Engineering Manager, Package') %>

## Issue boards

* [Planning](https://gitlab.com/groups/gitlab-org/-/boards/1196366?&label_name[]=devops%3A%3Apackage) - Issue board showing milestone planning for current and future milestones.
* [Assignments](https://gitlab.com/groups/gitlab-org/-/boards/1200765?&label_name[]=group%3A%3Apackage) - Issue board organized by assignee to give a snapshot of who is working on what.
* [Workflow](https://gitlab.com/groups/gitlab-org/-/boards/1284221?label_name[]=group%3A%3Apackage) - Issue board broken down by workflow label to give a snapshot of the status of in-progress issues.
* [Bugs](https://gitlab.com/groups/gitlab-org/-/boards/1200744?&label_name[]=bug&label_name[]=devops%3A%3Apackage) - Issue board displaying priority and severity of package related bugs.

## Async Daily Standups

The purpose of the daily standup is to allow team members to have visibility into what everyone else is doing, allow a platform for asking for and offering help, and provide a starting point for some social conversations. We use [geekbot](https://geekbot.com/) integrated with Slack.

While it is encouraged to participate in the full variety of daily questions, it is completely acceptable to skip questions by entering `-`.

A quick status of the various issues and merge requests that are currently being worked on is suggested. If the work is in progress, provide the estimated percentage complete, and the percentage confidence level in that estimate.

> `!12345 - 50% complete, 85% confident` - Working on tests and refactoring

This would mean that you are roughly halfway to being code complete with merge request 12345, and are fairly confident that your estimate is correct. A low confidence percentage is a way of identifying that you still believe there is much to uncover in what work is left.

It may be helpful to provide some commentary on what piece/aspect of the MR or issue you are working on as well. Once the MR is code complete and in review, you can leave off the percentages and simply say `in review` or `in maintainer review`.

## Weekly Retrospective

Our weekly retrospective is intended to provide the team an opportunity to retrospect on our week's effort. The discussion takes the usual GitLab asynchronous endabled synchronous meeting format: it has an agenda google document and we upload the video to [GitLab Unfiltered](https://youtube.com/c/GitLabUnfiltered). We currently have our retro every week on Friday morning (UTC-7) and, every 3rd week the meeting is held on Thursday afternoon (UTC-7) to support people in APAC TZs. The retrospective is a 25 minute long meeting.

The document is an ongoing list of retrospectives with a date heading as well as a link to the video after it has been uploaded. Each retrospective is divided into `what went not so well` and `what went well` in that order - so we can end the meeting on a positive note. In asynchronous style, we add our items prior to the meeting and read them out during the meeting. We read people's items when they aren't able to attend.

We roll up some of our retro thoughts into our monthly, milestone-linked, async retrospective. Ideally we will be able to address concerns in the retro. Action items are described during the meeting.

Examples of our retrospectives can be found here: [https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A/search?query=package+retro](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A/search?query=package+retro)

## Issue Weighting

| Weight | Description  |
| --- | --- | 
| 1: Trivial | The problem is very well understood, no extra investigation is required, the exact solution is already known and just needs to be implemented, no surprises are expected, and no coordination with other teams or people is required.<br><br>Examples are documentation updates, simple regressions, and other bugs that have already been investigated and discussed and can be fixed with a few lines of code, or technical debt that we know exactly how to address, but just haven't found time for yet.<br><br>This will map to a confidence greater or equal to 90%. |
| 2: Small | The problem is well understood and a solution is outlined, but a little bit of extra investigation will probably still be required to realize the solution. Few surprises are expected, if any, and no coordination with other teams or people is required.<br><br>Examples are simple features, like a new API endpoint to expose existing data or functionality, or regular bugs or performance issues where some investigation has already taken place.<br><br>This will map to a confidence greater than or equal to 75%. |
| 3: Medium | Features that are well understood and relatively straightforward. A solution will be outlined, and most edge cases will be considered, but some extra investigation will be required to realize the solution. Some surprises are expected, and coordination with other teams or people may be required.<br><br>Bugs that are relatively poorly understood and may not yet have a suggested solution. Significant investigation will definitely be required, but the expectation is that once the problem is found, a solution should be relatively straightforward.<br><br>Examples are regular features, potentially with a backend and frontend component, or most bugs or performance issues.<br><br>This will map to a confidence greater than or equal to 60%. |
| 5: Large | Features that are well understood, but known to be hard. A solution will be outlined, and major edge cases will be considered, but extra investigation will definitely be required to realize the solution. Many surprises are expected, and coordination with other teams or people is likely required.<br><br>Bugs that are very poorly understood, and will not have a suggested solution. Significant investigation will be required, and once the problem is found, a solution may not be straightforward.<br><br>Examples are large features with a backend and frontend component, or bugs or performance issues that have seen some initial investigation but have not yet been reproduced or otherwise "figured out".<br><br>This will map to a confidence greater than or equal to 50%. |

Anything larger than 5 should be broken down. Anything with a confidence percentage lower than 50% should be investigated prior to finalising the issue weight.

## Common Links

* [Package backend board]
* [#g_package] in Slack
* [Recorded meetings][youtube]
* [Retrospectives][retros]
* [Timezones][timezones]

[Package backend board]: https://gitlab.com/groups/gitlab-org/-/boards/892745
[#g_package]: https://gitlab.slack.com/archives/g_package
[youtube]: https://www.youtube.com/playlist?list=PL05JrBw4t0KoPiSySNHTfvxC20i0LppMf
[retros]: https://gitlab.com/gl-retrospectives/package/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=retrospective
[timezones]: https://timezone.io/team/gitlab-package-team
