<!--Bug Report-->
<!--Please remember this are suggestions. Any extra information you provide is beneficial.-->
<!-- Edit this file at : https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/.gitlab/issue_templates/Bug-Report.md -->
<!-- Template code owner: @brandon_lyon, marketing web designer/developer -->

### What is/are the relevant URLs or emails?

(Example: https://about.gitlab.com/something/etc/)

### Briefly describe the bug

(Example: A link is broken and when hovering over it the color is the wrong color and poorly aligned.)

### What are the steps to reproduce the bug

(Example (In a bulleted list):)

* Scroll to the W section of the page
* When condition X is true
* Hover over Y

### What is the observed behavior? Be specific.

(Example (In a bulleted list):)

* Hovering changes the color to red and adds padding/margins, as seen in the screenshot below
* Clicking on link A takes you to the wrong place

### What is the expected behavior? Be specific.

(Example (In a bulleted list):)

* Hovering changes the color to purple with no extra margins/padding
* Clicking on link A takes you to the url https://subdomain.domain.tld/path/etc/

### Please provide any relevant screenshots

(Example: Insert screenshot(s) here)

* [How to take a screenshot](https://www.howtogeek.com/205375/how-to-take-screenshots-on-almost-any-device/)

### What is your window size, browser, operating system, and versions

(Example: Chrome 77, OSX 10.15.0, 944px by 977px. [https://whatsmybrowser.org/b/MPC757K](https://whatsmybrowser.org/b/MPC757K))

[https://www.whatsmybrowser.org](https://www.whatsmybrowser.org/)

### What device are you using

(Example: Samsung Galaxy S 10 Plus)

### If applicable, what browser plugins might be related to the issue

(Example: Adblock, noscript, safe browsing)

### If applicable, what is your geographic location

(Example: Eurpoean Union is relevant to GDPR or cookie notices)
